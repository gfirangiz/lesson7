package Task1;

public class Car {

        private String make;
        private String model;
        private int year;
        private double rentalPrice;

        public String getMake () {

            return make;
        }
        public void setMake (String make) {
            this.make = make;
        }

        public String getModel () {

            return model;
        }
        public void setModel (String model) {
            this.model = model;
        }

        public int getYear () {

            return year;
        }
        public void setYear (int year) {
            this.year = year;
        }

        public double getRentalPrice () {

            return rentalPrice;
        }
        public void setrentalPrice (double rentalPrice) {
            this.rentalPrice = rentalPrice;
        }


}
