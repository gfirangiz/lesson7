package Task1;

import Task1.Car;

public class Main {

    public static void main(String[] args) {

    Car car = new Car();

    car.setMake ("BMW");
    car.setModel ("M5");
    car.setYear(2020);
    car.setrentalPrice(35000.0);

        System.out.println("Task1.Car details: ");
        System.out.println("Make: " + car.getMake());
        System.out.println("Model: " + car.getModel());
        System.out.println("Year: " + car.getYear());
        System.out.println("Rental Price: $" + car.getRentalPrice());

    }
}