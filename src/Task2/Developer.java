package Task2;

import Task2.Employee;

public class Developer extends Employee {

    private String programmingLanguage;

    public Developer(String name, int salary, String programmingLanguage) {
        super(name, salary);
        this.programmingLanguage = programmingLanguage;
    }

    public String getProgrammingLanguage() {
        return programmingLanguage;
    }

    public void setProgrammingLanguage(String programmingLanguage) {
        this.programmingLanguage = programmingLanguage;
    }

    @Override
    public void getDetails() {
        System.out.println("Task2.Employee's name is " + getName() + " and his salary is " + getSalary() + ". He use  " + getProgrammingLanguage() + ".");

    }
}
