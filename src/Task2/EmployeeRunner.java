package Task2;

import Task2.Developer;

public class EmployeeRunner {

    public static void main (String [] args) {
        Manager manager = new Manager("Asif",3000, "Information Technologies");
        manager.getDetails();
        Developer developer = new Developer("Ali", 1500,"Java");
        developer.getDetails();
    }
}
