package Task2;

import Task2.Employee;

public class Manager extends Employee {

    private String department;

    public Manager(String name, int salary, String department) {
        super(name, salary);
        this.department = department;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    @Override
    public void getDetails() {
        System.out.println("Task2.Employee's name is " + getName() + " and his salary is " + getSalary() + ". He works in " + getDepartment() + ".");

    }
}
