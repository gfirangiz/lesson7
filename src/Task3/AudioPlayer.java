package Task3;

public class AudioPlayer implements Playable{

    @Override
    public void play() {
        System.out.println("AudioPlayer is playing.");

    }

    @Override
    public void stop() {
        System.out.println("AudioPlayer has stoped.");

    }
}
