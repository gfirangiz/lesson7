package Task3;

public class VideoPlayer implements Playable{

    @Override
    public void play() {
        System.out.println("VideoPlayer is playing.");

    }

    @Override
    public void stop() {
        System.out.println("VideoPlayer has stoped.");

    }
}
