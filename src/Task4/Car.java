package Task4;

public abstract class Car implements Rentable {
    private String make;
    private String model;
    private int year;
    private double rentalRate;

    public Car (String make, String model, int year, double rentalRate) {
        this.make=make;
        this.model=model;
        this.year=year;
        this.rentalRate=rentalRate;
    }

    public String getMake () {

        return make;
    }
    public void setMake (String make) {
        this.make = make;
    }

    public String getModel () {

        return model;
    }
    public void setModel (String model) {
        this.model = model;
    }

    public int getYear () {

        return year;
    }
    public void setYear (int year) {
        this.year = year;
    }

    public double getRentalRate () {

        return rentalRate;
    }
    public void setRentalRate (double rentalRate) {
        this.rentalRate = rentalRate;
    }

    public abstract double calculateRentalCharge(int numDays);

    public void displayCarInfo() {
        System.out.println("The car's make is:  " + getMake());
        System.out.println("The car's model is: " + getModel());
        System.out.println("The car's year is: " + getYear());
        System.out.println("The car's rental Rate is: " + getRentalRate());
    }

    @Override
    public void rent(int numDays) {

    }

    @Override
    public void returnCar() {

    }

}
