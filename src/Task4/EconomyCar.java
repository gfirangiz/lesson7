package Task4;

public class EconomyCar extends Car{
    private double fuelEfficiency;

    public EconomyCar(String make, String model, int year, double rentalRate) {
        super(make, model, year, rentalRate);
    }

    public double getFuelEfficiency() {
        return fuelEfficiency;
    }

    public void setFuelEfficiency(double fuelEfficiency) {
        this.fuelEfficiency = fuelEfficiency;
    }

    @Override
    public double calculateRentalCharge(int numDays) {
        return numDays * getRentalRate();
    }

    @Override
    public void rent(int numDays) {
        System.out.println(getModel() + " rented for "+numDays+" days");
    }

    @Override
    public void returnCar() {
        System.out.println(getModel() + " returned");
    }
    @Override
    public void displayCarInfo(){
        super.displayCarInfo();
        System.out.println("Fuel Efficiency: "+ fuelEfficiency);
    }
}
