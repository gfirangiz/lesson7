package Task4;

import java.util.Arrays;

public class LuxuryCar extends Car{

    private String[] premiumFeatures;

    public LuxuryCar(String make, String model, int year, double rentalRate) {
        super(make, model, year, rentalRate);
    }

    public String[] getPremiumFeatures() {
        return premiumFeatures;
    }

    public void setPremiumFeatures(String[] premiumFeatures) {
        this.premiumFeatures = premiumFeatures;
    }

    @Override
    public double calculateRentalCharge(int numDays) {
        return numDays * getRentalRate();
    }

    @Override
    public void rent(int numDays) {
        System.out.println(getModel() + " rented for "+numDays+" days");
    }

    @Override
    public void returnCar() {
        System.out.println(getModel() + " returned");
    }
    @Override
    public void displayCarInfo(){
        super.displayCarInfo();
        System.out.println("Features: "+ Arrays.toString(premiumFeatures));
    }
}
