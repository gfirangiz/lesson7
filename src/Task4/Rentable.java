package Task4;

public interface Rentable {

    void rent (int numDays);

    void returnCar();
}
