package Task4;

public class RentalTransaction{

    private Car car;
    private String customerName;
    private int rentalDays;

    public RentalTransaction(Car car, String customerName, int rentalDays) {
        this.car = car;
        this.customerName = customerName;
        this.rentalDays = rentalDays;
    }
    public Car getCar() {
        return car;
    }

    public void setCar(String Car) {
        this.car = car;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getRentalDays() {
        return rentalDays;
    }

    public void setRentalDays(int rentalDays) {
        this.rentalDays = rentalDays;
    }

    private double calculateRentalCost(){
        return car.calculateRentalCharge(rentalDays);
    }
    public void displayTransactionInfo(){
        car.displayCarInfo();
        System.out.println("Customer name: "+customerName);
        System.out.println("Rental days: "+rentalDays);
        System.out.println("Rental total cost: " + calculateRentalCost());
    }
}
