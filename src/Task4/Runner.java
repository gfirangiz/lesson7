package Task4;

public class Runner {
    public static void main(String[] args) {
        EconomyCar polo = new EconomyCar("VW","Polo",2012,15.5);
        polo.setFuelEfficiency(23.9);
        RentalTransaction rentalPoloTransaction = new RentalTransaction(polo,"Fira",10);
        rentalPoloTransaction.displayTransactionInfo();
        polo.rent(9);
        polo.returnCar();

        System.out.println("_______________________________________");

        LuxuryCar bmw = new LuxuryCar("BWM","M5",2012,50);
        bmw.setPremiumFeatures(new String[] {"Cruse control","Sound isolation"});
        RentalTransaction rentalBmwTransaction = new RentalTransaction(bmw,"Zaur",5);
        rentalBmwTransaction.displayTransactionInfo();

        bmw.rent(7);
        bmw.returnCar();
    }

}
